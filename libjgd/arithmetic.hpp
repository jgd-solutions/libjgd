#pragma once

#include <concepts>

namespace jgd {

template <std::integral Integral>
inline constexpr bool is_odd(const Integral n) noexcept
{
  return n & 0b1;
}

template <std::integral Integral>
inline constexpr bool is_even(const Integral n) noexcept
{
  return !(n & 0b1);
}

// only well defined for positive inputs
inline constexpr auto divide_ceil(const std::integral auto numerator,
                                  const std::integral auto divisor)
{
  return (numerator + divisor - 1) / divisor;
}

}  // namespace jgd
