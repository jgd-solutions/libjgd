jgd_add_library(
  TYPE STATIC # until export macros are employed
  OUT_TARGET_NAME target
  PUBLIC_HEADERS arithmetic.hpp numeric_aliases.hpp
  SOURCES numeric_aliases.cpp)

if (LIBJGD_BUILD_TESTS)
  jgd_add_test_executable(EXECUTABLE arithmetic_ut SOURCES
    arithmetic.test.cpp LIBS ${target} Boost::ut)

  jgd_add_test_executable(EXECUTABLE numeric_aliases_ut SOURCES
    numeric_aliases.test.cpp LIBS ${target} Boost::ut)
endif ()
