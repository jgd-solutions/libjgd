#include <libjgd/numeric_aliases.hpp>

using namespace jgd;

u16 literals::operator"" _u16(ullong n) { return static_cast<u16>(n); }
