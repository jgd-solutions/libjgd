#include <boost/ut.hpp>
#include <libjgd/arithmetic.hpp>

int main() {
  using namespace boost::ut;
  using namespace jgd;

  "is_odd"_test = [] {
    expect(is_odd(3) == _b(true));
    expect(is_odd(2) == _b(false));
    expect(is_odd(0) == _b(false));
    expect(is_odd(-2) == _b(false));
    expect(is_odd(-3) == _b(true));
  };

  "is_even"_test = [] {
    expect(is_even(3) == _b(false));
    expect(is_even(2) == _b(true));
    expect(is_even(0) == _b(true));
    expect(is_even(-2) == _b(true));
    expect(is_even(-3) == _b(false));
  };

  "divide_ceil"_test = [] {
    expect(divide_ceil(10, 1) == 10_i);
    expect(divide_ceil(10, 2) == 5_i);
    expect(divide_ceil(10, 3) == 4_i);
    expect(divide_ceil(10, 4) == 3_i);
    expect(divide_ceil(20, 7) == 3_i);
    expect(divide_ceil(0, 7) == 0_i);
  };
}