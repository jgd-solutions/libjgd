#include <boost/ut.hpp>
#include <libjgd/numeric_aliases.hpp>

int main() {
  using namespace boost::ut;
  using namespace jgd;

  "u16 UDL"_test = [] { expect(3_u16 == 3_us); };
}
